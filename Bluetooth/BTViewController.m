//
//  BTViewController.m
//  Bluetooth
//
//  Created by Radu on 7/12/12.
//  Copyright (c) 2012 Radu Motisan. All rights reserved.
// http://www.pocketmagic.net/?p=2827

#import "BTViewController.h"
#import "BTAboutViewController.h"
#import <AudioToolbox/AudioToolbox.h>


@interface BTViewController ()

@end

@implementation BTViewController
int counter;

int showMessage(NSString *title, NSString *msg)
{
    UIAlertView *Alert = [[UIAlertView alloc]
                          initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [Alert show];
    return 1;
}

// handle tableview

@synthesize btDevItems;
@synthesize myTableView;

// build one listview cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *unview in cell.contentView.subviews) {
        [unview removeFromSuperview];
    }
    
    //cell.textLabel.text = [btDevItems objectAtIndex:indexPath.row];
    BTListDevItem *item = (BTListDevItem *)[btDevItems objectAtIndex:indexPath.row];
    
    UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(5, 0, 400, 20)];
    label1.text=item.name;
    label1.adjustsFontSizeToFitWidth=YES;
    [cell.contentView addSubview:label1];
    //[label release];
    
    UILabel *label2=[[UILabel alloc]initWithFrame:CGRectMake(5, 20, 400, 20)];
    label2.text=item.description;
    label2.textColor=[UIColor redColor];
    [label2 setFont:[UIFont systemFontOfSize:14.00]];
    [cell.contentView addSubview:label2];
    //[tipjob release];
    
    UIButton *connectbtn=[[UIButton alloc] initWithFrame:CGRectMake(myTableView.frame.size.width-60, 6, 50, 30)];
    [connectbtn setTitle:@"Connect" forState:UIControlStateNormal];
    [connectbtn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.00]];
    [connectbtn.titleLabel setTextColor:[UIColor whiteColor]];
    [connectbtn setTag:indexPath.row];
    [connectbtn setBackgroundColor:[UIColor lightGrayColor]];
    [connectbtn addTarget:self action:@selector(connecwithbluetoothdevice:) forControlEvents:UIControlEventTouchUpInside];
    [connectbtn.layer setCornerRadius:2.00];
    [connectbtn clipsToBounds];
    
    [cell.contentView addSubview:connectbtn];
    
    return cell;
}
-(void)connecwithbluetoothdevice:(id)sender
{
    BTListDevItem *item = (BTListDevItem *)[btDevItems objectAtIndex:[sender tag]];
    
    NSString *message = [NSString stringWithFormat:@"Device %@ [%@]", item.name, item.description];
    
    showMessage(@"Connect to:", message);
    
    [self deviceConnect :([sender tag])];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [btDevItems count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
  /*
    BTListDevItem *item = (BTListDevItem *)[btDevItems objectAtIndex:indexPath.row];
    
    NSString *message = [NSString stringWithFormat:@"Device %@ [%@]", item.name, item.description];
    
    showMessage(@"Connect to:", message);
    
    [self deviceConnect :( indexPath.row)];
    */
}

/* entry point */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [bluon_btn setBackgroundColor:[UIColor lightGrayColor]];
    [bluon_btn.layer setCornerRadius:3.00];
    [bluon_btn clipsToBounds];
    
    [bluoff_btn setBackgroundColor:[UIColor lightGrayColor]];
    [bluoff_btn.layer setCornerRadius:3.00];
    [bluoff_btn clipsToBounds];
    
    [scan_btn setBackgroundColor:[UIColor lightGrayColor]];
    [scan_btn.layer setCornerRadius:3.00];
    [scan_btn clipsToBounds];
    [clear_btn setBackgroundColor:[UIColor lightGrayColor]];
    [clear_btn.layer setCornerRadius:3.00];
    [clear_btn clipsToBounds];
    [about_btn setBackgroundColor:[UIColor lightGrayColor]];
    [about_btn.layer setCornerRadius:3.00];
    [about_btn clipsToBounds];
    
    
    // Init the btDevItems Array
	btDevItems = [[NSMutableArray alloc] init];
    
    // setup bluetooth interface
    btManager = [BluetoothManager sharedInstance];
    
    // setup bluetooth notifications
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(deviceDiscovered:)
     name:@"BluetoothDeviceDiscoveredNotification"
     object:nil];

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(bluetoothAvailabilityChanged:)
     name:@"BluetoothAvailabilityChangedNotification"
     object:nil];
    
    // global notification explorer
    CFNotificationCenterAddObserver(CFNotificationCenterGetLocalCenter(), 
                                    NULL, 
                                    MyCallBack, 
                                    NULL, 
                                    NULL,  
                                    CFNotificationSuspensionBehaviorDeliverImmediately);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(starttimerforsound) name:@"play" object:nil];
    
    
    // Create a view of the standard size at the top of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    //bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_ = [[GADBannerView alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height-GAD_SIZE_320x50.height, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];
    
    // Specify the ad unit ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
}
// global notification callback
void MyCallBack (CFNotificationCenterRef center,
                 void *observer,
                 CFStringRef name,
                 const void *object,
                 CFDictionaryRef userInfo) {
    NSLog(@"CFN Name:%@ Data:%@", name, userInfo);
    
    NSString *disconnectstring=[NSString stringWithFormat:@"CFN Name:%@ Data:%@", name, userInfo];
    
    if ([disconnectstring rangeOfString:@"BluetoothDeviceDisconnectSuccessNotification"].location == NSNotFound) {
        NSLog(@"string does not contain bla");
    } else {
        NSLog(@"string contains disconnection!");
       [[NSNotificationCenter defaultCenter] postNotificationName:@"play" object:nil];

    }
    
}
/* exit point */
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

/* Listview helper functions */
-(void) clearAllList {
    self.btDevItems = nil;
    [myTableView reloadData];
}

-(void) removeFromList:(NSInteger)index { 
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:btDevItems];
    [tempArray removeObjectAtIndex:index];
    btDevItems = tempArray;
}

/* Keep our GUI in portrait mode */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/* Interface actions - about */
- (IBAction)showAbout {
    // open the BTAboutViewController
//    BTAboutViewController *aboutView=[[BTAboutViewController alloc] init];
//    UINavigationController *navC=[[UINavigationController alloc] initWithRootViewController:aboutView];
//    [self presentViewController:navC animated:YES completion:nil];
}


/* Interface actions - clear listview */
- (IBAction)clearList {
    [self clearAllList];
}

/* Interface actions - scan */
- (IBAction)scanButtonAction {
    if ([btManager enabled]) {
        // clear listview
        [self clearAllList];
        // start scan
        [btManager  setDeviceScanningEnabled:YES];
    } else {
        showMessage(@"Error", @"Turn Bluetooth on first!");
    }

}

/* Interface actions - bt on */
- (IBAction)bluetoothON {
    NSLog(@"bluetoothON called.");
    [btManager setPowered:YES];
    [btManager setEnabled:YES]; 
    
}

/* Interface actions - bt off */
- (IBAction)bluetoothOFF {
    NSLog(@"bluetoothOFF called.");
    //BluetoothManager *manager = [BluetoothManager sharedInstance];
    [btManager setEnabled:NO]; 
    [btManager setPowered:NO];
}

/* Bluetooth notifications */
- (void)bluetoothAvailabilityChanged:(NSNotification *)notification {

    NSLog(@"NOTIFICATION:bluetoothAvailabilityChanged called. BT State: %d", [btManager enabled]);     
}

- (void)deviceDiscovered:(NSNotification *) notification {

    BluetoothDevice *bt = [notification object];
    
    NSLog(@"NOTIFICATION:deviceDiscovered: %@ %@",bt.name, bt.address);

    //create a new list item
    BTListDevItem *item = [[BTListDevItem alloc] initWithName:bt.name description:bt.address type:0 btdev:bt];
    
    //add it to list
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:btDevItems];
    [tempArray addObject:(item)];
    btDevItems = tempArray;
    [myTableView reloadData];
}

/* Bluetooth connectivity */
- (void)deviceConnect:(NSInteger)index { 
    BTListDevItem *item = (BTListDevItem *)[btDevItems objectAtIndex:index];
    NSLog(@"deviceConnect to %@", item.name);
    
    //BluetoothDevice 
    //  [btManager pairDevice:(item.description)];
    
    //  [btManager connectDevice:(item.description)];
    [item.btdev connect];
}
// Notificaiton sound & vibration
-(void)starttimerforsound
{
    [self clearAllList];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:TimerInterval target:self selector:@selector(playAlertSoundWithVib) userInfo:nil repeats:YES];//start thread to

}
- (void) playAlertSoundWithVib {
    
    counter++;
    if (counter>2) {
        
        [self.timer invalidate];
        counter=0;
    }
    // ivar
    SystemSoundID mBeep;
    
    // Create the sound ID
    NSString* path = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
    NSURL* url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &mBeep);
    
    // Play the sound
    AudioServicesPlaySystemSound(mBeep);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
   
}

/*
 2012-07-16 19:51:25.364 Bluetooth[706:707] deviceConnect to MOON-PC
 2012-07-16 19:51:25.366 Bluetooth[706:707] BTM: connecting to device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:27.743 Bluetooth[706:707] BTM: attempting to connect to service 0x00000010 on device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:27.751 Bluetooth[706:707] BTM: attempting to connect to service 0x00000008 on device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:28.994 Bluetooth[706:707] BTM: connection to service 0x00000010 on device "MOON-PC" C4:46:19:C6:39:D1 failed with error 305
 2012-07-16 19:51:30.286 Bluetooth[706:707] BTM: connection to service 0x00000008 on device "MOON-PC" C4:46:19:C6:39:D1 failed with error 305
 2012-07-16 19:51:55.841 Bluetooth[706:707] deviceConnect to MOON-PC
 2012-07-16 19:51:55.847 Bluetooth[706:707] BTM: connecting to device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:58.188 Bluetooth[706:707] BTM: attempting to connect to service 0x00000010 on device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:58.193 Bluetooth[706:707] BTM: attempting to connect to service 0x00000008 on device "MOON-PC" C4:46:19:C6:39:D1
 2012-07-16 19:51:59.360 Bluetooth[706:707] BTM: connection to service 0x00000010 on device "MOON-PC" C4:46:19:C6:39:D1 failed with error 305
 2012-07-16 19:52:00.668 Bluetooth[706:707] BTM: connection to service 0x00000008 on device "MOON-PC" C4:46:19:C6:39:D1 failed with error 305
 2012-07-16 19:53:43.483 Bluetooth[706:707] BTM: found device "Celluon " 00:18:E4:27:18:39
 2012-07-16 19:53:43.488 Bluetooth[706:707] NOTIFICATION:deviceDiscovered: Celluon  00:18:E4:27:18:39
 2012-07-16 19:53:45.727 Bluetooth[706:707] deviceConnect to Celluon 
 2012-07-16 19:53:45.732 Bluetooth[706:707] BTM: connecting to device "Celluon " 00:18:E4:27:18:39
 2012-07-16 19:53:47.204 Bluetooth[706:707] BTM: attempting to connect to service 0x00000020 on device "Celluon " 00:18:E4:27:18:39
 2012-07-16 19:53:47.216 Bluetooth[706:707] BTM: connection to service 0x00000020 on device "Celluon " 00:18:E4:27:18:39 failed with error 305
 */


@end
